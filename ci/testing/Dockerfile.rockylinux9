# SPDX-FileCopyrightText: 2021 Dalibo
#
# SPDX-License-Identifier: GPL-3.0-or-later

FROM rockylinux:9
RUN dnf install -y https://download.postgresql.org/pub/repos/yum/reporpms/EL-9-x86_64/pgdg-redhat-repo-latest.noarch.rpm
RUN dnf install -y epel-release
ARG PG_VERSION=12
RUN dnf install -y postgresql${PG_VERSION}-server \
	postgresql${PG_VERSION}-contrib \
	pg_stat_kcache_${PG_VERSION} \
	pg_qualstats_${PG_VERSION} \
	powa_${PG_VERSION} \
	pgbackrest
WORKDIR /tmp
ARG ETCD_VER=v3.5.4
ARG ETCD_DOWNLOAD_URL=https://storage.googleapis.com/etcd
RUN curl -O -L \
	${ETCD_DOWNLOAD_URL}/${ETCD_VER}/etcd-${ETCD_VER}-linux-amd64.tar.gz
RUN tar xzvf etcd-${ETCD_VER}-linux-amd64.tar.gz --strip-components=1
RUN mv etcd /usr/bin/
RUN mv etcdctl /usr/bin/
ARG PGE_VERSION=0.15.0
ARG PGE_FNAME=postgres_exporter-${PGE_VERSION}.linux-amd64
ARG PGE_TGZ=${PGE_FNAME}.tar.gz
RUN curl -O -L \
	https://github.com/prometheus-community/postgres_exporter/releases/download/v${PGE_VERSION}/${PGE_TGZ}
RUN tar xvf ${PGE_TGZ}
RUN mv ${PGE_FNAME}/postgres_exporter /usr/bin/
RUN dnf install -y gcc git jq openssl procps-ng python-pip

RUN dnf install -y epel-release
RUN dnf install -y https://yum.dalibo.org/labs/dalibo-labs-4-1.noarch.rpm
RUN dnf install -y temboard-agent

RUN pip3 install tox
RUN useradd --uid 5432 --create-home runner

RUN touch /etc/port-for.conf
RUN chown runner: /etc/port-for.conf

USER runner
WORKDIR /home/runner
ENV PATH=/usr/pgsql-${PG_VERSION}/bin:$PATH
