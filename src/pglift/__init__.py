# SPDX-FileCopyrightText: 2021 Dalibo
#
# SPDX-License-Identifier: GPL-3.0-or-later

from __future__ import annotations

import sys
from functools import cache
from importlib import metadata
from typing import Any, Callable, Final, TypeVar, overload

import pluggy

from . import hookspecs as h
from . import pm, settings
from ._compat import ParamSpec

__all__ = ["h", "hookimpl"]

# Declare type for hookimpl on our side until a version (> 1.0.0) is
# available.

F = TypeVar("F", bound=Callable[..., Any])


@overload
def hookimpl(__func: F) -> F:
    ...


@overload
def hookimpl(**kwargs: Any) -> Callable[[F], F]:
    ...


def hookimpl(*args: Any, **kwargs: Any) -> Any:
    return pluggy.HookimplMarker(__name__)(*args, **kwargs)


def version() -> str:
    return metadata.version(__name__)


@cache
def plugin_manager(s: settings.Settings) -> pm.PluginManager:
    return pm.PluginManager.get(s)


R = TypeVar("R")
P = ParamSpec("P")


def hooks(
    s: settings.Settings, spec: Callable[P, R], /, *args: P.args, **kwargs: P.kwargs
) -> list[R]:
    """Invoke hook implementations matching 'spec' and return their result."""
    pm = plugin_manager(s)
    assert not args
    opts = pm.parse_hookspec_opts(sys.modules[spec.__module__], spec.__name__)
    assert (
        opts is None or not opts["firstresult"]
    ), f"hook {spec.__name__!r} has firstresult=True"
    fn = getattr(pm.hook, spec.__name__)
    return fn(**kwargs)  # type: ignore[no-any-return]


def hook(
    s: settings.Settings, spec: Callable[P, R], /, *args: P.args, **kwargs: P.kwargs
) -> R:
    """Invoke hook implementations matching 'spec' and return the first result, if any."""
    pm = plugin_manager(s)
    assert not args
    opts = pm.parse_hookspec_opts(sys.modules[spec.__module__], spec.__name__)
    assert (
        opts is not None and opts["firstresult"]
    ), f"hook {spec.__name__!r} hasn't firstresult=True"
    fn = getattr(pm.hook, spec.__name__)
    return fn(**kwargs)  # type: ignore[no-any-return]


execpath: Final = (
    sys.executable
    if getattr(sys, "frozen", False)
    else f"{sys.executable} -m {__name__}"
)
