# SPDX-FileCopyrightText: 2021 Dalibo
#
# SPDX-License-Identifier: GPL-3.0-or-later

from __future__ import annotations

from typing import Any

from . import h, hooks
from .settings import Settings


def do(settings: Settings, env: dict[str, Any] | None = None, header: str = "") -> None:
    if env is None:
        env = {}
    hooks(
        settings,
        h.site_configure_install,
        settings=settings,
        header=header,
        env=env,
    )


def undo(settings: Settings) -> None:
    hooks(settings, h.site_configure_uninstall, settings=settings)


def check(settings: Settings, *, partial: bool = False) -> bool:
    """Check the installation.

    Return True if the installation is complete (or partial, if partial=True).
    """
    results = (
        status
        for outcome in hooks(
            settings, h.site_configure_check, settings=settings, log=not partial
        )
        for status in outcome
    )
    if partial:
        return any(results)
    return all(results)
