# SPDX-FileCopyrightText: 2021 Dalibo
#
# SPDX-License-Identifier: GPL-3.0-or-later

from __future__ import annotations

import functools
from collections.abc import Sequence
from typing import IO, Any

import click
import psycopg
from pydantic.v1.utils import deep_update

from .. import databases, postgresql, privileges, task
from ..models import helpers, interface, system
from .util import (
    Group,
    Obj,
    OutputFormat,
    dry_run_option,
    instance_identifier_option,
    model_dump,
    output_format_option,
    pass_instance,
    print_argspec,
    print_json_for,
    print_schema,
    print_table_for,
)


@click.group("database", cls=Group)
@instance_identifier_option
@click.option(
    "--schema",
    is_flag=True,
    callback=functools.partial(print_schema, model=interface.Database),
    expose_value=False,
    is_eager=True,
    help="Print the JSON schema of database model and exit.",
)
@click.option(
    "--ansible-argspec",
    is_flag=True,
    callback=functools.partial(print_argspec, model=interface.Database),
    expose_value=False,
    is_eager=True,
    hidden=True,
    help="Print the Ansible argspec of database model and exit.",
)
def cli(**kwargs: Any) -> None:
    """Manage databases."""


@cli.command("create")
@helpers.parameters_from_model(interface.Database, "create")
@pass_instance
@click.pass_obj
def create(obj: Obj, instance: system.Instance, database: interface.Database) -> None:
    """Create a database in a PostgreSQL instance"""
    with obj.lock, postgresql.running(instance):
        if databases.exists(instance, database.name):
            raise click.ClickException("database already exists")
        with task.transaction():
            databases.apply(instance, database)


@cli.command("alter")  # type: ignore[arg-type]
@helpers.parameters_from_model(interface.Database, "update", parse_model=False)
@click.argument("dbname")
@pass_instance
@click.pass_obj
def alter(obj: Obj, instance: system.Instance, dbname: str, **changes: Any) -> None:
    """Alter a database in a PostgreSQL instance"""
    with obj.lock, postgresql.running(instance):
        values = databases.get(instance, dbname).model_dump()
        values = deep_update(values, changes)
        altered = interface.Database.model_validate(values)
        databases.apply(instance, altered)


@cli.command("apply", hidden=True)
@click.option("-f", "--file", type=click.File("r"), metavar="MANIFEST", required=True)
@output_format_option
@dry_run_option
@pass_instance
@click.pass_obj
def apply(
    obj: Obj,
    instance: system.Instance,
    file: IO[str],
    output_format: OutputFormat,
    dry_run: bool,
) -> None:
    """Apply manifest as a database"""
    database = interface.Database.parse_yaml(file)
    if dry_run:
        ret = interface.ApplyResult(change_state=None)
    else:
        with obj.lock, postgresql.running(instance):
            ret = databases.apply(instance, database)
    if output_format == OutputFormat.json:
        print_json_for(ret)


@cli.command("get")
@output_format_option
@click.argument("name")
@pass_instance
def get(instance: system.Instance, name: str, output_format: OutputFormat) -> None:
    """Get the description of a database"""
    with postgresql.running(instance):
        db = databases.get(instance, name)
    if output_format == OutputFormat.json:
        print_json_for(model_dump(db))
    else:
        print_table_for(
            [db],
            functools.partial(model_dump, exclude={"extensions", "schemas"}),
            box=None,
        )


@cli.command("list")
@output_format_option
@click.argument("dbname", nargs=-1)
@pass_instance
def ls(
    instance: system.Instance, dbname: Sequence[str], output_format: OutputFormat
) -> None:
    """List databases (all or specified ones)

    Only queried databases are shown when DBNAME is specified.
    """

    with postgresql.running(instance):
        dbs = databases.ls(instance, dbnames=dbname)
    if output_format == OutputFormat.json:
        print_json_for([model_dump(db) for db in dbs])
    else:
        print_table_for(dbs, model_dump)


@cli.command("drop")
@helpers.parameters_from_model(interface.DatabaseDropped, "create")
@pass_instance
@click.pass_obj
def drop(
    obj: Obj, instance: system.Instance, databasedropped: interface.DatabaseDropped
) -> None:
    """Drop a database"""
    with obj.lock, postgresql.running(instance):
        databases.drop(instance, databasedropped)


@cli.command("privileges")
@click.argument("name")
@click.option("-r", "--role", "roles", multiple=True, help="Role to inspect")
@click.option("--default", "defaults", is_flag=True, help="Display default privileges")
@output_format_option
@pass_instance
def list_privileges(
    instance: system.Instance,
    name: str,
    roles: Sequence[str],
    defaults: bool,
    output_format: OutputFormat,
) -> None:
    """List privileges on a database."""
    with postgresql.running(instance):
        databases.get(instance, name)  # check existence
        try:
            prvlgs = privileges.get(
                instance, databases=(name,), roles=roles, defaults=defaults
            )
        except ValueError as e:
            raise click.ClickException(str(e)) from None
    if output_format == OutputFormat.json:
        print_json_for([model_dump(p) for p in prvlgs])
    else:
        print_table_for(prvlgs, model_dump)


@cli.command("run")
@click.argument("sql_command")
@click.option(
    "-d", "--database", "dbnames", multiple=True, help="Database to run command on"
)
@click.option(
    "-x",
    "--exclude-database",
    "exclude_dbnames",
    multiple=True,
    help="Database to not run command on",
)
@output_format_option
@pass_instance
def run(
    instance: system.Instance,
    sql_command: str,
    dbnames: Sequence[str],
    exclude_dbnames: Sequence[str],
    output_format: OutputFormat,
) -> None:
    """Run given command on databases of a PostgreSQL instance"""
    with postgresql.running(instance):
        try:
            result = databases.run(
                instance,
                sql_command,
                dbnames=dbnames,
                exclude_dbnames=exclude_dbnames,
            )
        except psycopg.ProgrammingError as e:
            raise click.ClickException(str(e)) from None
    if output_format == OutputFormat.json:
        print_json_for(result)
    else:
        for dbname, rows in result.items():
            print_table_for(rows, lambda m: m, title=f"Database {dbname}")


@cli.command("dump")
@click.argument("dbname")
@pass_instance
def dump(instance: system.Instance, dbname: str) -> None:
    """Dump a database"""
    with postgresql.running(instance):
        databases.dump(instance, dbname)
