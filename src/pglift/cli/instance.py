# SPDX-FileCopyrightText: 2021 Dalibo
#
# SPDX-License-Identifier: GPL-3.0-or-later

from __future__ import annotations

import os
from collections.abc import Sequence
from functools import partial
from typing import IO, Any, Callable

import click
from pydantic.v1.utils import deep_update

from .. import h, hooks, instances, plugin_manager, postgresql, privileges, task
from ..models import helpers, interface, system
from ..settings import Settings
from ..settings._postgresql import PostgreSQLVersion
from ..types import Status
from .util import (
    Callback,
    CompositeCommandGroup,
    Obj,
    OutputFormat,
    PluggableCommandGroup,
    _list_instances,
    dry_run_option,
    foreground_option,
    instance_lookup,
    model_dump,
    output_format_option,
    pass_settings,
    print_argspec,
    print_json_for,
    print_schema,
    print_table_for,
)


def instance_identifier(
    nargs: int = 1, required: bool = False
) -> Callable[[Callback], Callback]:
    def decorator(fn: Callback) -> Callback:
        command = click.argument(
            "instance",
            nargs=nargs,
            required=required,
            callback=instance_lookup,
            shell_complete=_list_instances,
        )(fn)
        assert command.__doc__
        command.__doc__ += (
            "\n\n    INSTANCE identifies target instance as <version>/<name> where the "
            "<version>/ prefix may be omitted if there is only one instance "
            "matching <name>."
        )
        if not required:
            command.__doc__ += " Required if there is more than one instance on system."
        return command

    return decorator


class InstanceCommands(
    PluggableCommandGroup, CompositeCommandGroup[interface.Instance]
):
    """Group for 'instance' sub-commands handling some of them that require a
    composite interface.Instance model built from registered plugins at
    runtime.
    """

    model = interface.Instance

    def register_plugin_commands(self, obj: Obj) -> None:
        hooks(obj.settings, h.instance_cli, group=self)


def print_instance_schema(
    context: click.Context, param: click.Parameter, value: bool
) -> None:
    settings = context.obj.settings
    pm = plugin_manager(settings)
    return print_schema(context, param, value, model=interface.Instance.composite(pm))


def print_instance_argspec(
    context: click.Context, param: click.Parameter, value: bool
) -> None:
    settings = context.obj.settings
    model = interface.Instance.composite(plugin_manager(settings))
    print_argspec(context, param, value, model=model)


@click.group(cls=InstanceCommands)
@click.option(
    "--schema",
    is_flag=True,
    callback=print_instance_schema,
    expose_value=False,
    is_eager=True,
    help="Print the JSON schema of instance model and exit.",
)
@click.option(
    "--ansible-argspec",
    is_flag=True,
    callback=print_instance_argspec,
    expose_value=False,
    is_eager=True,
    hidden=True,
    help="Print the Ansible argspec of instance model and exit.",
)
def cli() -> None:
    """Manage instances."""


# Help mypy because click.group() looses the type of 'cls' argument.
assert isinstance(cli, InstanceCommands)


@cli.command_with_composite_model("create")
def _create(Instance: type[interface.Instance]) -> Callback:
    @helpers.parameters_from_model(Instance, "create")
    @click.option(
        "--drop-on-error/--no-drop-on-error",
        default=True,
        help=(
            "On error, drop partially initialized instance by possibly "
            "rolling back operations (true by default)."
        ),
    )
    @pass_settings
    @click.pass_obj
    def command(
        obj: Obj,
        settings: Settings,
        instance: interface.Instance,
        drop_on_error: bool,
    ) -> None:
        """Initialize a PostgreSQL instance"""
        with obj.lock:
            if instances.exists(instance.name, instance.version, settings):
                raise click.ClickException("instance already exists")
            with task.transaction(drop_on_error):
                instances.apply(settings, instance)

    return command


@cli.command_with_composite_model("alter")
def _alter(Instance: type[interface.Instance]) -> Callback:
    @instance_identifier(nargs=1)  # type: ignore[arg-type]
    @helpers.parameters_from_model(Instance, "update", parse_model=False)
    @pass_settings
    @click.pass_obj
    def command(
        obj: Obj, settings: Settings, instance: system.Instance, **changes: Any
    ) -> None:
        """Alter PostgreSQL INSTANCE"""
        with obj.lock:
            status = postgresql.status(instance)
            values = instances._get(instance, status).model_dump(exclude={"settings"})
            values = deep_update(values, changes)
            altered = Instance.model_validate(values)
            instances.apply(settings, altered, _is_running=status == Status.running)

    return command


@cli.command("apply", hidden=True)
@click.option("-f", "--file", type=click.File("r"), metavar="MANIFEST", required=True)
@output_format_option
@dry_run_option
@pass_settings
@click.pass_obj
def apply(
    obj: Obj,
    settings: Settings,
    file: IO[str],
    output_format: OutputFormat,
    dry_run: bool,
) -> None:
    """Apply manifest as a PostgreSQL instance"""
    pm = plugin_manager(settings)
    model = interface.Instance.composite(pm)
    instance = model.parse_yaml(file)
    if dry_run:
        ret = interface.InstanceApplyResult(change_state=None)
    else:
        with obj.lock:
            ret = instances.apply(settings, instance)
    if output_format == OutputFormat.json:
        print_json_for(ret)


@cli.command("promote")
@instance_identifier(nargs=1)
@click.pass_obj
def promote(obj: Obj, instance: system.Instance) -> None:
    """Promote standby PostgreSQL INSTANCE"""
    with obj.lock:
        instances.promote(instance)


@cli.command("get")
@output_format_option
@instance_identifier(nargs=1)
def get(instance: system.Instance, output_format: OutputFormat) -> None:
    """Get the description of PostgreSQL INSTANCE.

    Unless --output-format is specified, 'settings' and 'state' fields are not
    shown as well as 'standby' information if INSTANCE is not a standby.
    """
    i = instances.get(instance)
    if output_format == OutputFormat.json:
        print_json_for(model_dump(i))
    else:
        exclude = {
            "settings",
            "state",
            "data_directory",
            "wal_directory",
            "powa",
        }
        if not instance.standby:
            exclude.add("standby")
        print_table_for([i], partial(model_dump, exclude=exclude), box=None)


@cli.command("list")
@click.option(
    "--version",
    type=click.Choice(list(PostgreSQLVersion)),
    help="Only list instances of specified version.",
)
@output_format_option
@pass_settings
def ls(
    settings: Settings,
    version: PostgreSQLVersion | None,
    output_format: OutputFormat,
) -> None:
    """List the available instances"""
    items = instances.ls(settings, version=version)
    if output_format == OutputFormat.json:
        print_json_for([model_dump(m) for m in items])
    else:
        print_table_for(items, model_dump)


@cli.command("drop")
@instance_identifier(nargs=-1)
@click.pass_obj
def drop(obj: Obj, instance: tuple[system.Instance, ...]) -> None:
    """Drop PostgreSQL INSTANCE"""
    with obj.lock:
        for i in instance:
            instances.drop(i)


@cli.command("status")
@instance_identifier(nargs=1)
@click.pass_context
def status(context: click.Context, instance: system.Instance) -> None:
    """Check the status of instance and all satellite components.

    Output the status string value ('running', 'not running') for each component.
    If not all services are running, the command exit code will be 3.
    """
    exit_code = Status.running.value
    results = list(
        filter(None, hooks(instance._settings, h.instance_status, instance=instance))
    )
    for status, component in reversed(results):
        if status != Status.running:
            exit_code = Status.not_running.value
        click.echo(f"{component}: {status.name.replace('_', ' ')}")
    context.exit(exit_code)


@cli.command("start")
@instance_identifier(nargs=-1)
@foreground_option
@click.option("--all", "all_instances", is_flag=True, help="Start all instances.")
@click.pass_obj
def start(
    obj: Obj,
    instance: tuple[system.Instance, ...],
    foreground: bool,
    all_instances: bool,
) -> None:
    """Start PostgreSQL INSTANCE"""
    if foreground and len(instance) != 1:
        raise click.UsageError(
            "only one INSTANCE argument may be given with --foreground"
        )
    with obj.lock:
        for i in instance:
            instances.start(i, foreground=foreground)


@cli.command("stop")
@instance_identifier(nargs=-1)
@click.option("--all", "all_instances", is_flag=True, help="Stop all instances.")
@click.pass_obj
def stop(obj: Obj, instance: tuple[system.Instance, ...], all_instances: bool) -> None:
    """Stop PostgreSQL INSTANCE"""
    with obj.lock:
        for i in instance:
            instances.stop(i)


@cli.command("reload")
@instance_identifier(nargs=-1)
@click.option("--all", "all_instances", is_flag=True, help="Reload all instances.")
@click.pass_obj
def reload(
    obj: Obj, instance: tuple[system.Instance, ...], all_instances: bool
) -> None:
    """Reload PostgreSQL INSTANCE"""
    with obj.lock:
        for i in instance:
            instances.reload(i)


@cli.command("restart")
@instance_identifier(nargs=-1)
@click.option("--all", "all_instances", is_flag=True, help="Restart all instances.")
@click.pass_obj
def restart(
    obj: Obj, instance: tuple[system.Instance, ...], all_instances: bool
) -> None:
    """Restart PostgreSQL INSTANCE"""
    with obj.lock:
        for i in instance:
            instances.restart(i)


@cli.command("exec")
@instance_identifier(nargs=1, required=True)
@click.argument("command", required=True, nargs=-1, type=click.UNPROCESSED)
def exec(instance: system.Instance, command: tuple[str, ...]) -> None:
    """Execute command in the libpq environment for PostgreSQL INSTANCE.

    COMMAND parts may need to be prefixed with -- to separate them from
    options when confusion arises.
    """
    instances.exec(instance, command)


def get_shell(context: click.Context, param: click.Parameter, value: str | None) -> str:
    if value is None:
        try:
            value = os.environ["SHELL"]
        except KeyError:
            raise click.UsageError(
                f"SHELL environment variable not found; try to use {'/'.join(param.opts)} option"
            ) from None
    return value


@cli.command("shell")
@instance_identifier(nargs=1)
@click.option(
    "--shell",
    type=click.Path(exists=True, dir_okay=False),
    callback=get_shell,
    help="Path to shell executable",
)
def shell(instance: system.Instance, shell: str) -> None:
    """Start a shell with instance environment.

    Unless --shell option is specified, the $SHELL environment variable is
    used to guess which shell executable to use.
    """
    env = instances.env_for(instance, path=True)
    click.echo(f"Starting {shell!r} with {instance} environment")
    os.execle(shell, shell, os.environ | env)  # nosec


@cli.command("env")
@instance_identifier(nargs=1)
@output_format_option
def env(instance: system.Instance, output_format: OutputFormat) -> None:
    """Output environment variables suitable to handle to PostgreSQL INSTANCE.

    This can be injected in shell using:

        export $(pglift instance env myinstance)
    """
    instance_env = instances.env_for(instance, path=True)
    if output_format == OutputFormat.json:
        print_json_for(instance_env)
    else:
        for key, value in sorted(instance_env.items()):
            click.echo(f"{key}={value}")


@cli.command("logs")
@click.option("--follow/--no-follow", "-f/", default=False, help="Follow log output.")
@instance_identifier(nargs=1)
def logs(instance: system.Instance, follow: bool) -> None:
    """Output PostgreSQL logs of INSTANCE.

    This assumes that the PostgreSQL instance is configured to use file-based
    logging (i.e. log_destination amongst 'stderr' or 'csvlog').
    """
    if follow:
        logstream = postgresql.logs(instance)
    else:
        logstream = postgresql.logs(instance, timeout=0)
    try:
        for line in logstream:
            click.echo(line, nl=False)
    except TimeoutError:
        pass
    except FileNotFoundError as e:
        raise click.ClickException(str(e)) from None


@cli.command("privileges")
@instance_identifier(nargs=1)
@click.option(
    "-d",
    "--database",
    "databases",
    multiple=True,
    help="Database to inspect. When not provided, all databases are inspected.",
)
@click.option("-r", "--role", "roles", multiple=True, help="Role to inspect")
@click.option("--default", "defaults", is_flag=True, help="Display default privileges")
@output_format_option
def list_privileges(
    instance: system.Instance,
    databases: Sequence[str],
    roles: Sequence[str],
    defaults: bool,
    output_format: OutputFormat,
) -> None:
    """List privileges on INSTANCE's databases."""
    with postgresql.running(instance):
        try:
            prvlgs = privileges.get(
                instance, databases=databases, roles=roles, defaults=defaults
            )
        except ValueError as e:
            raise click.ClickException(str(e)) from None
    if output_format == OutputFormat.json:
        print_json_for([model_dump(p) for p in prvlgs])
    else:
        if defaults:
            title = f"Default privileges on instance {instance}"
        else:
            title = f"Privileges on instance {instance}"
        print_table_for(prvlgs, model_dump, title=title)


@cli.command("upgrade")
@instance_identifier(nargs=1)
@click.option(
    "--version",
    "newversion",
    type=click.Choice(list(PostgreSQLVersion)),
    help="PostgreSQL version of the new instance (default to site-configured value).",
)
@click.option(
    "--name", "newname", help="Name of the new instance (default to old instance name)."
)
@click.option(
    "--port", required=False, type=click.INT, help="Port of the new instance."
)
@click.option(
    "--jobs",
    required=False,
    type=click.INT,
    help="Number of simultaneous processes or threads to use (from pg_upgrade).",
)
@click.pass_obj
def upgrade(
    obj: Obj,
    instance: system.Instance,
    newversion: PostgreSQLVersion | None,
    newname: str | None,
    port: int | None,
    jobs: int | None,
) -> None:
    """Upgrade INSTANCE using pg_upgrade"""
    with obj.lock:
        postgresql.check_status(instance, Status.not_running)
        new_instance = instances.upgrade(
            instance, version=newversion, name=newname, port=port, jobs=jobs
        )
        instances.start(new_instance)
