# SPDX-FileCopyrightText: 2021 Dalibo
#
# SPDX-License-Identifier: GPL-3.0-or-later

from __future__ import annotations

from collections.abc import Sequence
from functools import partial
from typing import IO, Any

import click
from pydantic.v1.utils import deep_update

from .. import plugin_manager, postgresql, privileges, roles
from ..models import helpers, interface, system
from ..settings import Settings
from .util import (
    Callback,
    CompositeCommandGroup,
    Obj,
    OutputFormat,
    dry_run_option,
    instance_identifier_option,
    model_dump,
    output_format_option,
    pass_instance,
    print_argspec,
    print_json_for,
    print_schema,
    print_table_for,
)


def print_role_schema(
    context: click.Context, param: click.Parameter, value: bool
) -> None:
    pm = plugin_manager(context.obj.settings)
    model = interface.Role.composite(pm)
    return print_schema(context, param, value, model=model)


def print_role_argspec(
    context: click.Context, param: click.Parameter, value: bool
) -> None:
    settings: Settings = context.obj.settings
    model = interface.Role.composite(plugin_manager(settings))
    print_argspec(context, param, value, model=model)


class RoleCommands(CompositeCommandGroup[interface.Role]):
    """Group for 'role' sub-commands handling some of them that require a
    composite interface.Role model built from registered plugins at
    runtime.
    """

    model = interface.Role


@click.group("role", cls=RoleCommands)
@instance_identifier_option
@click.option(
    "--schema",
    is_flag=True,
    callback=print_role_schema,
    expose_value=False,
    is_eager=True,
    help="Print the JSON schema of role model and exit.",
)
@click.option(
    "--ansible-argspec",
    is_flag=True,
    callback=print_role_argspec,
    expose_value=False,
    is_eager=True,
    hidden=True,
    help="Print the Ansible argspec of role model and exit.",
)
def cli(**kwargs: Any) -> None:
    """Manage roles."""


# Help mypy because click.group() looses the type of 'cls' argument.
assert isinstance(cli, RoleCommands)


@cli.command_with_composite_model("create")
def _create(Role: type[interface.Role]) -> Callback:
    @helpers.parameters_from_model(Role, "create")
    @pass_instance
    @click.pass_obj
    def command(obj: Obj, instance: system.Instance, role: interface.Role) -> None:
        """Create a role in a PostgreSQL instance"""
        with obj.lock, postgresql.running(instance):
            if roles.exists(instance, role.name):
                raise click.ClickException("role already exists")
            roles.apply(instance, role)

    return command


@cli.command_with_composite_model("alter")
def _alter(Role: type[interface.Role]) -> Callback:
    @helpers.parameters_from_model(Role, "update", parse_model=False)  # type: ignore[arg-type]
    @click.argument("rolname")
    @pass_instance
    @click.pass_obj
    def command(
        obj: Obj, instance: system.Instance, rolname: str, **changes: Any
    ) -> None:
        """Alter a role in a PostgreSQL instance"""
        with obj.lock, postgresql.running(instance):
            values = roles.get(instance, rolname).model_dump()
            values = deep_update(values, changes)
            altered = Role.model_validate(values)
            roles.apply(instance, altered)

    return command


@cli.command("apply", hidden=True)
@click.option("-f", "--file", type=click.File("r"), metavar="MANIFEST", required=True)
@output_format_option
@dry_run_option
@pass_instance
@click.pass_obj
def apply(
    obj: Obj,
    instance: system.Instance,
    file: IO[str],
    output_format: OutputFormat,
    dry_run: bool,
) -> None:
    """Apply manifest as a role"""
    pm = plugin_manager(instance._settings)
    model = interface.Role.composite(pm)
    role = model.parse_yaml(file)
    if dry_run:
        ret = interface.ApplyResult(change_state=None)
    else:
        with obj.lock, postgresql.running(instance):
            ret = roles.apply(instance, role)
    if output_format == OutputFormat.json:
        print_json_for(ret)


@cli.command("list")
@output_format_option
@pass_instance
def ls(instance: system.Instance, output_format: OutputFormat) -> None:
    """List roles in instance"""
    with postgresql.running(instance):
        rls = roles.ls(instance)
    if output_format == OutputFormat.json:
        print_json_for([model_dump(r) for r in rls])
    else:
        print_table_for(rls, partial(model_dump, exclude={"pgpass"}))


@cli.command("get")
@output_format_option
@click.argument("name")
@pass_instance
def get(instance: system.Instance, name: str, output_format: OutputFormat) -> None:
    """Get the description of a role"""
    with postgresql.running(instance):
        r = roles.get(instance, name)
    if output_format == OutputFormat.json:
        print_json_for(model_dump(r))
    else:
        print_table_for([r], model_dump, box=None)


@cli.command("drop")
@helpers.parameters_from_model(interface.RoleDropped, "create")
@pass_instance
def drop(instance: system.Instance, roledropped: interface.RoleDropped) -> None:
    """Drop a role"""
    with postgresql.running(instance):
        roles.drop(instance, roledropped)


@cli.command("privileges")
@click.argument("name")
@click.option(
    "-d", "--database", "databases", multiple=True, help="Database to inspect"
)
@click.option("--default", "defaults", is_flag=True, help="Display default privileges")
@output_format_option
@pass_instance
def list_privileges(
    instance: system.Instance,
    name: str,
    databases: Sequence[str],
    defaults: bool,
    output_format: OutputFormat,
) -> None:
    """List privileges of a role."""
    with postgresql.running(instance):
        roles.get(instance, name)  # check existence
        try:
            prvlgs = privileges.get(
                instance, databases=databases, roles=(name,), defaults=defaults
            )
        except ValueError as e:
            raise click.ClickException(str(e)) from None
    if output_format == OutputFormat.json:
        print_json_for([model_dump(p) for p in prvlgs])
    else:
        print_table_for(prvlgs, model_dump)
