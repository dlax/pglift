# SPDX-FileCopyrightText: 2021 Dalibo
#
# SPDX-License-Identifier: GPL-3.0-or-later

import functools

from pydantic import Field, ValidationInfo, field_validator

from ... import types


class RESTAPI(types.BaseModel):
    connect_address: types.Address = Field(
        default_factory=functools.partial(types.local_address, port=8008),
        description="IP address (or hostname) and port, to access the Patroni's REST API.",
    )
    listen: types.Address = Field(
        default_factory=types.unspecified_address,
        description="IP address (or hostname) and port that Patroni will listen to for the REST API. Defaults to connect_address if not provided.",
        validate_default=True,
    )

    @field_validator("listen", mode="before")
    @classmethod
    def __validate_listen_(cls, value: str, info: ValidationInfo) -> str:
        """Set 'listen' from 'connect_address' if unspecified.

        >>> RESTAPI()  # doctest: +ELLIPSIS
        RESTAPI(connect_address='...:8008', listen='...:8008')
        >>> RESTAPI(connect_address="localhost:8008")
        RESTAPI(connect_address='localhost:8008', listen='localhost:8008')
        >>> RESTAPI(connect_address="localhost:8008", listen="server:123")
        RESTAPI(connect_address='localhost:8008', listen='server:123')
        """
        if not value:
            value = info.data["connect_address"]
            assert isinstance(value, str)
        return value
