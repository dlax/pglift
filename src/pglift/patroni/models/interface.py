# SPDX-FileCopyrightText: 2021 Dalibo
#
# SPDX-License-Identifier: GPL-3.0-or-later

import socket
from typing import Annotated, Optional

from pydantic import Field, FilePath, SecretStr, field_validator

from ... import types
from .common import RESTAPI


class ClusterMember(types.BaseModel, extra="allow", frozen=True):
    """An item of the list of members returned by Patroni API /cluster endpoint."""

    host: str
    name: str
    port: int
    role: str
    state: str


class ClientSSLOptions(types.BaseModel):
    cert: FilePath = Field(description="Client certificate.")
    key: FilePath = Field(description="Private key.")
    password: Optional[SecretStr] = Field(
        default=None, description="Password for the private key."
    )


class ClientAuth(types.BaseModel):
    ssl: Optional[ClientSSLOptions] = Field(
        default=None,
        description="Client certificate options.",
    )


class PostgreSQL(types.BaseModel):
    connect_host: Optional[str] = Field(
        default=None,
        description="Host or IP address through which PostgreSQL is externally accessible.",
    )
    replication: Optional[ClientAuth] = Field(
        default=None,
        description="Authentication options for client (libpq) connections to remote PostgreSQL by the replication user.",
    )
    rewind: Optional[ClientAuth] = Field(
        default=None,
        description="Authentication options for client (libpq) connections to remote PostgreSQL by the rewind user.",
    )


class Etcd(types.BaseModel):
    username: str = Field(
        description="Username for basic authentication to etcd.",
    )
    password: SecretStr = Field(
        description="Password for basic authentication to etcd."
    )


class ServiceManifest(types.ServiceManifest, service_name="patroni"):
    # XXX Or simply use instance.qualname?
    cluster: str = Field(
        description="Name (scope) of the Patroni cluster.",
        json_schema_extra={"readOnly": True},
    )
    node: str = Field(
        default_factory=socket.getfqdn,
        description="Name of the node (usually the host name).",
        json_schema_extra={"readOnly": True},
    )
    restapi: RESTAPI = Field(
        default_factory=RESTAPI, description="REST API configuration"
    )

    postgresql: Optional[PostgreSQL] = Field(
        default=None,
        description="Configuration for PostgreSQL setup and remote connection.",
    )
    etcd: Optional[Etcd] = Field(
        default=None, description="Instance-specific options for etcd DCS backend."
    )
    cluster_members: Annotated[
        list[ClusterMember], types.CLIConfig(hide=True), types.AnsibleConfig(hide=True)
    ] = Field(
        default=[],
        description="Members of the Patroni this instance is member of.",
        json_schema_extra={"readOnly": True},
    )

    __validate_none_values_ = field_validator("node", "restapi", mode="before")(
        classmethod(types.default_if_none)
    )
