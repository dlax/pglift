# SPDX-FileCopyrightText: 2021 Dalibo
#
# SPDX-License-Identifier: GPL-3.0-or-later

import typing
from decimal import Decimal
from typing import Annotated, Literal, Optional

import psycopg.conninfo
from pydantic import Field, SecretStr, field_validator

from ..types import AnsibleConfig, CLIConfig, Manifest

WALSenderState = Literal["startup", "catchup", "streaming", "backup", "stopping"]


def walsender_state(value: str) -> WALSenderState:
    assert value in typing.get_args(
        WALSenderState
    ), f"unexpected WAL sender state {value!r}"
    return value  # type: ignore[return-value]


class Standby(Manifest):
    """Standby information."""

    primary_conninfo: Annotated[str, CLIConfig(name="for")] = Field(
        description="DSN of primary for streaming replication.",
        json_schema_extra={"readOnly": True},
    )
    password: Optional[SecretStr] = Field(
        default=None,
        description="Password for the replication user.",
        exclude=True,
        json_schema_extra={"readOnly": True},
    )
    status: Annotated[Literal["demoted", "promoted"], CLIConfig(hide=True)] = Field(
        default="demoted",
        description="Instance standby state.",
        json_schema_extra={"writeOnly": True},
        exclude=True,
    )
    slot: Optional[str] = Field(
        None,
        description="Replication slot name. Must exist on primary.",
        json_schema_extra={"readOnly": True},
    )
    replication_lag: Annotated[
        Optional[Decimal], CLIConfig(hide=True), AnsibleConfig(hide=True)
    ] = Field(
        default=None,
        description="Replication lag.",
        json_schema_extra={"readOnly": True},
    )
    wal_sender_state: Annotated[
        Optional[WALSenderState], CLIConfig(hide=True), AnsibleConfig(hide=True)
    ] = Field(
        default=None,
        description="State of the WAL sender process (on primary) this standby is connected to.",
        json_schema_extra={"readOnly": True},
    )

    @field_validator("primary_conninfo")
    @classmethod
    def __validate_primary_conninfo_(cls, value: str) -> str:
        """Validate 'primary_conninfo' field.

        >>> Standby.model_validate({"primary_conninfo": "host=localhost"})  # doctest: +ELLIPSIS
        Standby(primary_conninfo='host=localhost', password=None, ...)
        >>> Standby.model_validate({"primary_conninfo": "hello"})
        Traceback (most recent call last):
          ...
        pydantic_core._pydantic_core.ValidationError: 1 validation error for Standby
        primary_conninfo
          Value error, missing "=" after "hello" in connection info string
         [type=value_error, input_value='hello', input_type=str]
            ...
        >>> Standby.model_validate({"primary_conninfo": "host=localhost password=xx"})
        Traceback (most recent call last):
          ...
        pydantic_core._pydantic_core.ValidationError: 1 validation error for Standby
        primary_conninfo
          Value error, connection string must not contain a password [type=value_error, input_value='host=localhost password=xx', input_type=str]
            ...
        """
        try:
            conninfo = psycopg.conninfo.conninfo_to_dict(value)
        except psycopg.ProgrammingError as e:
            raise ValueError(str(e)) from e
        if "password" in conninfo:
            raise ValueError("connection string must not contain a password")
        return value

    @property
    def full_primary_conninfo(self) -> str:
        """Connection string to the primary, including password.

        >>> s = Standby.model_validate({"primary_conninfo": "host=primary port=5444", "password": "qwerty"})
        >>> s.full_primary_conninfo
        'host=primary port=5444 password=qwerty'
        """
        kw = {}
        if self.password:
            kw["password"] = self.password.get_secret_value()
        return psycopg.conninfo.make_conninfo(self.primary_conninfo, **kw)
