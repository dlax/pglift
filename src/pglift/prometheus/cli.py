# SPDX-FileCopyrightText: 2021 Dalibo
#
# SPDX-License-Identifier: GPL-3.0-or-later

from __future__ import annotations

from functools import partial
from typing import IO

import click

from .. import exceptions, prometheus, task
from ..cli.util import (
    Group,
    Obj,
    OutputFormat,
    dry_run_option,
    foreground_option,
    output_format_option,
    pass_component_settings,
    pass_settings,
    print_argspec,
    print_json_for,
    print_schema,
)
from ..models import helpers, interface
from ..settings import Settings, _prometheus
from . import impl, models

pass_prometheus_settings = partial(
    pass_component_settings, prometheus, "Prometheus postgres_exporter"
)


@click.group("postgres_exporter", cls=Group)
@click.option(
    "--schema",
    is_flag=True,
    callback=partial(print_schema, model=models.PostgresExporter),
    expose_value=False,
    is_eager=True,
    help="Print the JSON schema of postgres_exporter model and exit.",
)
@click.option(
    "--ansible-argspec",
    is_flag=True,
    callback=partial(print_argspec, model=models.PostgresExporter),
    expose_value=False,
    is_eager=True,
    hidden=True,
    help="Print the Ansible argspec of postgres_exporter model and exit.",
)
def postgres_exporter() -> None:
    """Handle Prometheus postgres_exporter"""


@postgres_exporter.command("apply")
@click.option("-f", "--file", type=click.File("r"), metavar="MANIFEST", required=True)
@output_format_option
@dry_run_option
@pass_prometheus_settings
@pass_settings
@click.pass_obj
def postgres_exporter_apply(
    obj: Obj,
    settings: Settings,
    prometheus_settings: _prometheus.Settings,
    file: IO[str],
    output_format: OutputFormat,
    dry_run: bool,
) -> None:
    """Apply manifest as a Prometheus postgres_exporter."""
    exporter = models.PostgresExporter.parse_yaml(file)
    if dry_run:
        ret = interface.ApplyResult(change_state=None)
    else:
        with obj.lock:
            ret = impl.apply(exporter, settings, prometheus_settings)
    if output_format == OutputFormat.json:
        print_json_for(ret)


@postgres_exporter.command("install")
@helpers.parameters_from_model(models.PostgresExporter, "create")
@pass_prometheus_settings
@pass_settings
@click.pass_obj
def postgres_exporter_install(
    obj: Obj,
    settings: Settings,
    prometheus_settings: _prometheus.Settings,
    postgresexporter: models.PostgresExporter,
) -> None:
    """Install the service for a (non-local) instance."""
    with obj.lock, task.transaction():
        impl.apply(postgresexporter, settings, prometheus_settings)


@postgres_exporter.command("uninstall")
@click.argument("name")
@pass_settings
@click.pass_obj
def postgres_exporter_uninstall(obj: Obj, settings: Settings, name: str) -> None:
    """Uninstall the service."""
    with obj.lock:
        impl.drop(settings, name)


@postgres_exporter.command("start")
@click.argument("name")
@foreground_option
@pass_prometheus_settings
@pass_settings
@click.pass_obj
def postgres_exporter_start(
    obj: Obj,
    settings: Settings,
    prometheus_settings: _prometheus.Settings,
    name: str,
    foreground: bool,
) -> None:
    """Start postgres_exporter service NAME.

    The NAME argument is a local identifier for the postgres_exporter
    service. If the service is bound to a local instance, it should be
    <version>-<name>.
    """
    with obj.lock:
        service = impl.system_lookup(name, prometheus_settings)
        if service is None:
            raise exceptions.InstanceNotFound(name)
        impl.start(settings, service, foreground=foreground)


@postgres_exporter.command("stop")
@click.argument("name")
@pass_prometheus_settings
@pass_settings
@click.pass_obj
def postgres_exporter_stop(
    obj: Obj, settings: Settings, prometheus_settings: _prometheus.Settings, name: str
) -> None:
    """Stop postgres_exporter service NAME.

    The NAME argument is a local identifier for the postgres_exporter
    service. If the service is bound to a local instance, it should be
    <version>-<name>.
    """
    with obj.lock:
        service = impl.system_lookup(name, prometheus_settings)
        if service is None:
            raise exceptions.InstanceNotFound(name)
        impl.stop(settings, service)
