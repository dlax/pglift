# SPDX-FileCopyrightText: 2021 Dalibo
#
# SPDX-License-Identifier: GPL-3.0-or-later

from collections.abc import Mapping
from pathlib import Path
from typing import Annotated, ClassVar, Final, Literal, Optional

import psycopg
import psycopg.conninfo
from attrs import frozen
from pydantic import Field, SecretStr, field_validator

from .. import exceptions, types
from ..settings._prometheus import Settings
from ..types import Port
from . import impl

default_port: Final = 9187
service_name: Final = "postgres_exporter"


@frozen
class Config:
    values: Mapping[str, str]
    path: Path

    def __getitem__(self, key: str) -> str:
        try:
            return self.values[key]
        except KeyError as e:
            raise exceptions.ConfigurationError(self.path, f"{key} not found") from e


@frozen
class Service:
    """A Prometheus postgres_exporter service bound to a PostgreSQL instance."""

    __service_name__: ClassVar[str] = service_name

    name: str
    """Identifier for the service, usually the instance qualname."""

    settings: Settings

    port: int
    """TCP port for the web interface and telemetry."""

    password: Optional[SecretStr]

    def __str__(self) -> str:
        return f"{self.__service_name__}@{self.name}"

    def args(self) -> list[str]:
        config = impl._config(impl._configpath(self.name, self.settings))
        return impl._args(self.settings.execpath, config)

    def pidfile(self) -> Path:
        return impl._pidfile(self.name, self.settings)

    def env(self) -> dict[str, str]:
        config = impl._config(impl._configpath(self.name, self.settings))
        return impl._env(config)


class ServiceManifest(types.ServiceManifest, service_name="prometheus"):
    port: Port = Field(
        default=default_port,
        description="TCP port for the web interface and telemetry of Prometheus",
    )
    password: Optional[SecretStr] = Field(
        default=None,
        description="Password of PostgreSQL role for Prometheus postgres_exporter.",
        exclude=True,
    )

    @field_validator("password")
    @classmethod
    def __validate_password_(cls, v: Optional[SecretStr]) -> Optional[SecretStr]:
        """Validate 'password' field.

        >>> ServiceManifest(password='without_space')  # doctest: +ELLIPSIS
        ServiceManifest(...)
        >>> ServiceManifest(password='with space')  # doctest: +ELLIPSIS
        Traceback (most recent call last):
          ...
        pydantic_core._pydantic_core.ValidationError: 1 validation error for ServiceManifest
        password
          Value error, password must not contain blank spaces [type=value_error, input_value='with space', input_type=str]
            ...
        """
        # Avoid spaces as this will break postgres_exporter configuration.
        # See https://github.com/prometheus-community/postgres_exporter/issues/393
        if v is not None and " " in v.get_secret_value():
            raise ValueError("password must not contain blank spaces")
        return v


class PostgresExporter(types.Manifest):
    """Prometheus postgres_exporter service."""

    name: str = Field(description="locally unique identifier of the service")
    dsn: str = Field(description="connection string of target instance")
    password: Optional[SecretStr] = Field(
        description="connection password", default=None
    )
    port: int = Field(description="TCP port for the web interface and telemetry")
    state: Annotated[
        Literal["started", "stopped", "absent"],
        types.CLIConfig(choices=["started", "stopped"]),
    ] = Field(default="started", description="runtime state")

    @field_validator("name")
    @classmethod
    def __validate_name_(cls, v: str) -> str:
        """Validate 'name' field.

        >>> PostgresExporter(name='without-slash', dsn="", port=12)  # doctest: +ELLIPSIS
        PostgresExporter(name='without-slash', ...)
        >>> PostgresExporter(name='with/slash', dsn="", port=12)
        Traceback (most recent call last):
          ...
        pydantic_core._pydantic_core.ValidationError: 1 validation error for PostgresExporter
        name
          Value error, must not contain slashes [type=value_error, input_value='with/slash', input_type=str]
            ...
        """
        # Avoid slash as this will break file paths during settings templating
        # (configpath, etc.)
        if "/" in v:
            raise ValueError("must not contain slashes")
        return v

    @field_validator("dsn")
    @classmethod
    def __validate_dsn_(cls, value: str) -> str:
        try:
            psycopg.conninfo.conninfo_to_dict(value)
        except psycopg.ProgrammingError as e:
            raise ValueError(str(e)) from e
        return value
