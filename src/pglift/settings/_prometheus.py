# SPDX-FileCopyrightText: 2021 Dalibo
#
# SPDX-License-Identifier: GPL-3.0-or-later

import warnings
from pathlib import Path
from typing import Annotated, Any, Optional

from pydantic import AfterValidator, Field, FilePath, ValidationInfo, field_validator

from .base import BaseModel, ConfigPath, RunPath, TemplatedPath


class Settings(BaseModel):
    """Settings for Prometheus postgres_exporter"""

    execpath: FilePath = Field(description="Path to the postgres_exporter executable.")

    role: str = Field(
        default="prometheus",
        description="Name of the PostgreSQL role for Prometheus postgres_exporter.",
    )

    configpath: Annotated[
        Path, AfterValidator(TemplatedPath("name")), ConfigPath
    ] = Field(
        default=Path("prometheus/postgres_exporter-{name}.conf"),
        description="Path to the config file.",
        validate_default=True,
    )

    queriespath: Annotated[Optional[Path], ConfigPath] = Field(
        default=None,
        description="Path to the queries file (DEPRECATED).",
    )

    @field_validator("queriespath")
    @classmethod
    def __queriespath_is_deprecated_(cls, value: Any, info: ValidationInfo) -> Any:
        if value is not None:
            warnings.warn(
                f"{info.field_name!r} setting is deprecated; make sure the postgres_exporter in use supports this",
                FutureWarning,
                stacklevel=2,
            )
        return value

    pid_file: Annotated[Path, AfterValidator(TemplatedPath("name")), RunPath] = Field(
        default=Path("prometheus/{name}.pid"),
        description="Path to which postgres_exporter process PID will be written.",
        validate_default=True,
    )
