# SPDX-FileCopyrightText: 2021 Dalibo
#
# SPDX-License-Identifier: GPL-3.0-or-later

from pathlib import Path
from typing import Annotated, Literal

from pydantic import AfterValidator, AnyHttpUrl, Field, FilePath

from .base import (
    BaseModel,
    ConfigPath,
    DataPath,
    LogPath,
    RunPath,
    ServerCert,
    TemplatedPath,
)

Plugin = Literal[
    "activity",
    "administration",
    "dashboard",
    "maintenance",
    "monitoring",
    "pgconf",
    "statements ",
]


class Settings(BaseModel):
    """Settings for temBoard agent"""

    ui_url: AnyHttpUrl = Field(description="URL of the temBoard UI.")

    signing_key: FilePath = Field(
        description="Path to the public key for UI connection."
    )

    certificate: ServerCert = Field(
        description="TLS certificate files for the temboard-agent HTTP server."
    )

    execpath: FilePath = Field(
        default=Path("/usr/bin/temboard-agent"),
        description="Path to the temboard-agent executable.",
    )

    role: str = Field(
        default="temboardagent",
        description="Name of the PostgreSQL role for temBoard agent.",
    )

    configpath: Annotated[
        Path, AfterValidator(TemplatedPath("name")), ConfigPath
    ] = Field(
        default=Path("temboard-agent/temboard-agent-{name}.conf"),
        description="Path to the config file.",
        validate_default=True,
    )

    pid_file: Annotated[Path, AfterValidator(TemplatedPath("name")), RunPath] = Field(
        default=Path("temboard-agent/temboard-agent-{name}.pid"),
        description="Path to which temboard-agent process PID will be written.",
        validate_default=True,
    )

    plugins: tuple[Plugin, ...] = Field(
        default=("monitoring", "dashboard", "activity"),
        description="Plugins to load.",
    )

    home: Annotated[Path, AfterValidator(TemplatedPath("name")), DataPath] = Field(
        default=Path("temboard-agent/{name}"),
        description="Path to agent home directory containing files used to store temporary data",
        validate_default=True,
    )

    logpath: Annotated[Path, LogPath] = Field(
        default=Path("temboard"),
        description="Path where log files are stored.",
    )

    logmethod: Literal["stderr", "syslog", "file"] = Field(
        default="stderr", description="Method used to send the logs."
    )

    loglevel: Literal["DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"] = Field(
        default="INFO", description="Log level."
    )
