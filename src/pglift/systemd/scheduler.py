# SPDX-FileCopyrightText: 2021 Dalibo
#
# SPDX-License-Identifier: GPL-3.0-or-later

from __future__ import annotations

from typing import Literal

from .. import hookimpl
from ..settings import Settings
from . import disable, enable
from . import get_settings as s
from . import start, stop


def register_if(settings: Settings) -> bool:
    return settings.scheduler == "systemd"


def unit(service: str, qualname: str) -> str:
    return f"pglift-{service}@{qualname}.timer"


@hookimpl
def schedule_service(settings: Settings, service: str, name: str) -> Literal[True]:
    enable(s(settings), unit(service, name))
    return True


@hookimpl
def unschedule_service(
    settings: Settings, service: str, name: str, now: bool | None
) -> Literal[True]:
    kwargs = {}
    if now is not None:
        kwargs["now"] = now
    disable(s(settings), unit(service, name), **kwargs)
    return True


@hookimpl
def start_timer(settings: Settings, service: str, name: str) -> Literal[True]:
    start(s(settings), unit(service, name))
    return True


@hookimpl
def stop_timer(settings: Settings, service: str, name: str) -> Literal[True]:
    stop(s(settings), unit(service, name))
    return True
