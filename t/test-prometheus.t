# SPDX-FileCopyrightText: 2023 Dalibo
#
# SPDX-License-Identifier: GPL-3.0-or-later

  $ export SETTINGS="@$TMPDIR/config.json"
  $ cat > $TMPDIR/config.json <<EOF
  > {
  >   "cli": {
  >     "log_format": "%(levelname)-4s %(message)s"
  >   },
  >   "prefix": "$TMPDIR",
  >   "run_prefix": "$TMPDIR/run",
  >   "prometheus": {
  >     "execpath": "/usr/bin/prometheus-postgres-exporter"
  >   }
  > }
  > EOF

  $ PGEPORT=$(port-for prometheus)

  $ alias pglift="pglift --log-level=info"

  $ pglift site-configure install
  INFO creating PostgreSQL log directory
  $ trap "pglift --non-interactive postgres_exporter uninstall test; \
  >   pglift --non-interactive site-configure uninstall; \
  >   port-for -u prometheus" \
  >   EXIT

  $ pglift postgres_exporter install test dbname=monitoring $PGEPORT
  INFO configuring Prometheus postgres_exporter test
  INFO starting Prometheus postgres_exporter test
  $ pglift postgres_exporter stop test
  INFO stopping Prometheus postgres_exporter test
  $ pglift postgres_exporter start test
  INFO starting Prometheus postgres_exporter test
  $ cat > $TMPDIR/prometheus.yaml <<EOF
  > name: test
  > port: $PGEPORT
  > dsn: dbname=monitoring user=prometheus
  > EOF
  $ pglift postgres_exporter apply -f $TMPDIR/prometheus.yaml -o json --dry-run
  {
    "change_state": null
  }
  $ pglift postgres_exporter apply -f $TMPDIR/prometheus.yaml -o json
  INFO reconfiguring Prometheus postgres_exporter test
  INFO restarting Prometheus postgres_exporter test
  INFO starting Prometheus postgres_exporter test
  {
    "change_state": "changed"
  }

(cleanup)
  INFO dropping postgres_exporter service 'test'
  INFO stopping Prometheus postgres_exporter test
  INFO deleting PostgreSQL log directory (no-eol)
