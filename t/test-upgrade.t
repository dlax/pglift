# SPDX-FileCopyrightText: 2023 Dalibo
#
# SPDX-License-Identifier: GPL-3.0-or-later

Site settings:

  $ cat > $TMPDIR/config.json <<EOF
  > {
  >   "cli": {
  >     "log_format": "%(levelname)-4s %(message)s"
  >   },
  >   "prefix": "$TMPDIR",
  >   "run_prefix": "$TMPDIR/run"
  > }
  > EOF

  $ export SETTINGS="@$TMPDIR/config.json"
  $ pglift site-settings -o json | jq '.run_prefix, .prefix'
  "$TMPDIR/run"
  "$TMPDIR"

  $ alias pglift="pglift --non-interactive --log-level=info"

  $ pglift site-configure install
  INFO creating PostgreSQL log directory

  $ trap "pglift --non-interactive instance drop old new; \
  >   pglift --non-interactive site-configure uninstall; \
  >   port-for -u pg1; \
  >   port-for -u pg2" \
  >   EXIT

Define ports:

  $ PG1PORT=$(port-for pg1)
  $ PG2PORT=$(port-for pg2)

Create an instance, and upgrade it:

  $ pglift instance create old --port=$PG1PORT
  INFO initializing PostgreSQL
  INFO configuring PostgreSQL authentication
  INFO configuring PostgreSQL
  INFO starting PostgreSQL 1\d-old (re)
  $ pglift instance upgrade --name=new --port=$PG2PORT --jobs=3
  Error: instance is running
  [1]
  $ pglift instance stop old
  INFO stopping PostgreSQL 1\d-old (re)
  $ pglift instance upgrade --name=new --port=$PG2PORT --jobs=3
  INFO upgrading PostgreSQL instance
  INFO initializing PostgreSQL
  INFO configuring PostgreSQL authentication
  INFO configuring PostgreSQL
  INFO configuring PostgreSQL
  INFO starting PostgreSQL 1\d-new (re)

(cleanup)
  INFO dropping instance 1\d\/old (re)
  WARNING instance 1\d\/old is already stopped (re)
  INFO deleting PostgreSQL cluster
  INFO dropping instance 1\d\/new (re)
  INFO stopping PostgreSQL 1\d-new (re)
  INFO deleting PostgreSQL cluster
  INFO deleting PostgreSQL log directory (no-eol)
