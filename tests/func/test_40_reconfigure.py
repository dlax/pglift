# SPDX-FileCopyrightText: 2021 Dalibo
#
# SPDX-License-Identifier: GPL-3.0-or-later

from __future__ import annotations

import urllib.parse
from collections.abc import Iterator
from dataclasses import dataclass, field
from pathlib import Path
from typing import Any

import psutil
import pytest

from pglift import instances, postgresql, roles, systemd
from pglift.models import interface, system
from pglift.pgbackrest import base as pgbackrest
from pglift.prometheus import impl as prometheus
from pglift.settings import Settings, _postgresql
from pglift.systemd import service_manager
from pglift.temboard import impl as temboard

from . import AuthType, config_dict, connect, passfile_entries, role_in_pgpass


def get_last_start(settings: Settings, unit: str, pidfile: Path) -> float:
    if settings.service_manager == "systemd":
        assert settings.systemd
        _, value = systemd.get_property(
            settings.systemd, unit, "ActiveEnterTimestampMonotonic"
        ).split("=", 1)
        return float(value.strip())
    else:
        with pidfile.open() as f:
            return psutil.Process(int(f.readline().strip())).create_time()


@dataclass
class Recorder:
    service: Any

    def record(self) -> None:
        pass

    def assert_restarted(self) -> None:
        pytest.skip(f"{self.service.service_name} is not available")


@dataclass
class RestartRecorder(Recorder):
    instance: system.Instance
    records: list[float] = field(default_factory=list)

    def record(self) -> None:
        settings = self.instance._settings
        s = self.service.get_settings(settings)
        record = get_last_start(
            settings,
            service_manager.unit(self.service.service_name, self.instance.qualname),
            self.service._pidfile(self.instance.qualname, s),
        )
        self.records.append(record)

    def assert_restarted(self) -> None:
        assert self.records[-2] != self.records[-1]


@pytest.fixture(scope="module")
def prometheus_restart_recorder(
    instance: system.Instance,
    prometheus_execpath: Path | None,
) -> Recorder:
    if prometheus_execpath:
        return RestartRecorder(prometheus, instance)
    return Recorder(prometheus)


@pytest.fixture(scope="module")
def temboard_restart_recorder(
    instance: system.Instance,
    temboard_execpath: Path | None,
) -> Recorder:
    if temboard_execpath:
        return RestartRecorder(temboard, instance)
    return Recorder(temboard)


@pytest.fixture(scope="module")
def newport(tmp_port_factory: Iterator[int]) -> int:
    return next(tmp_port_factory)


role1, role2, role3 = (
    interface.Role(name="r1", password="1", **{"pgpass": True}),
    interface.Role(name="r2", password="2", **{"pgpass": True}),
    interface.Role(name="r3", **{"pgpass": False}),
)


@pytest.fixture(scope="module")
def passfile_roles(
    settings: Settings,
    instance: system.Instance,
    instance_manifest: interface.Instance,
    postgresql_auth: AuthType,
    postgresql_settings: _postgresql.Settings,
) -> None:
    if postgresql_auth == AuthType.pgpass:
        surole = instance_manifest.surole(settings)
        assert settings.postgresql.surole.pgpass
        assert postgresql.is_running(instance)
        roles.apply(instance, role1)
        roles.apply(instance, role2)
        roles.apply(instance, role3)
        port = instance.port
        passfile = postgresql_settings.auth.passfile
        assert passfile is not None
        assert role_in_pgpass(passfile, role1, port=port)
        assert role_in_pgpass(passfile, role2, port=port)
        assert not role_in_pgpass(passfile, role3)
        assert role_in_pgpass(passfile, surole, port=port)


@pytest.fixture(scope="module")
def reconfigured(
    instance: system.Instance,
    instance_manifest: interface.Instance,
    prometheus_restart_recorder: Recorder,
    temboard_restart_recorder: Recorder,
    passfile_roles: None,
    newport: int,
) -> Iterator[system.Instance]:
    update = {
        "port": newport,
        "restart_on_changes": True,
        "settings": {"lc_numeric": ""},
    }
    prometheus_restart_recorder.record()
    temboard_restart_recorder.record()

    changes = instances.configure(instance, instance_manifest._copy_validate(update))
    prometheus_restart_recorder.record()
    temboard_restart_recorder.record()

    yield instance

    restored_changes = instances.configure(instance, instance_manifest)
    changes.clear()
    changes.update(restored_changes)


def test_pgpass(
    settings: Settings,
    passfile: Path,
    reconfigured: system.Instance,
    surole_password: str,
    pgbackrest_password: str,
    pgbackrest_available: bool,
    newport: int,
) -> None:
    backuprole = settings.postgresql.backuprole.name
    assert f"*:{newport}:*:postgres:{surole_password}" in passfile_entries(passfile)
    if pgbackrest_available:
        assert f"*:{newport}:*:{backuprole}:{pgbackrest_password}" in passfile_entries(
            passfile, role=backuprole
        )


def test_get_locale(reconfigured: system.Instance) -> None:
    with connect(reconfigured) as conn:
        assert instances.get_locale(conn) is None


def test_passfile(
    settings: Settings,
    reconfigured: system.Instance,
    instance_manifest: interface.Instance,
    passfile: Path,
    newport: int,
) -> None:
    surole = instance_manifest.surole(settings)
    oldport = instance_manifest.port
    assert not role_in_pgpass(passfile, role1, port=oldport)
    assert role_in_pgpass(passfile, role1, port=newport)
    assert not role_in_pgpass(passfile, role2, port=oldport)
    assert role_in_pgpass(passfile, role2, port=newport)
    assert not role_in_pgpass(passfile, role3)
    assert not role_in_pgpass(passfile, surole, port=oldport)
    assert role_in_pgpass(passfile, surole, port=newport)


def test_pgbackrest(
    settings: Settings,
    reconfigured: system.Instance,
    pgbackrest_available: bool,
    newport: int,
) -> None:
    if not pgbackrest_available:
        pytest.skip("pgbackrest is not available")
    stanza = f"mystanza-{reconfigured.name}"
    pgbackrest_settings = pgbackrest.get_settings(settings)
    stanza_configpath = pgbackrest_settings.configpath / "conf.d" / f"{stanza}.conf"
    config_after = stanza_configpath.read_text()
    assert f"pg1-port = {newport}" in config_after.splitlines()


def test_prometheus(
    settings: Settings,
    reconfigured: system.Instance,
    prometheus_password: str,
    prometheus_restart_recorder: Recorder,
    prometheus_execpath: Path | None,
    newport: int,
) -> None:
    prometheus_restart_recorder.assert_restarted()
    name = reconfigured.qualname
    prometheus_settings = prometheus.get_settings(settings)
    configpath = Path(str(prometheus_settings.configpath).format(name=name))
    new_prometheus_config = config_dict(configpath)
    dsn = new_prometheus_config["DATA_SOURCE_NAME"]
    assert f"{urllib.parse.quote(prometheus_password)}@:{newport}" in dsn


def test_temboard(
    settings: Settings,
    reconfigured: system.Instance,
    temboard_execpath: Path | None,
    temboard_restart_recorder: Recorder,
    newport: int,
) -> None:
    temboard_restart_recorder.assert_restarted()
    temboard_settings = temboard.get_settings(settings)
    configpath = Path(
        str(temboard_settings.configpath).format(name=reconfigured.qualname)
    )
    lines = configpath.read_text().splitlines()
    assert f"port = {newport}" in lines
