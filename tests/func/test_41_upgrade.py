# SPDX-FileCopyrightText: 2021 Dalibo
#
# SPDX-License-Identifier: GPL-3.0-or-later

from __future__ import annotations

from pathlib import Path

from pglift import databases, postgresql
from pglift.models import system
from pglift.settings import Settings
from pglift.types import Status

from . import passfile_entries


def test_upgrade(pg_version: str, upgraded_instance: system.Instance) -> None:
    assert upgraded_instance.name == "upgraded"
    assert upgraded_instance.version == pg_version
    assert postgresql.status(upgraded_instance) == Status.not_running
    with postgresql.running(upgraded_instance):
        assert databases.exists(upgraded_instance, "postgres")


def test_upgrade_pgpass(
    settings: Settings,
    passfile: Path,
    upgraded_instance: system.Instance,
    surole_password: str | None,
    pgbackrest_password: str | None,
) -> None:
    backuprole = settings.postgresql.backuprole.name
    port = upgraded_instance.port
    assert f"*:{port}:*:postgres:{surole_password}" in passfile_entries(passfile)
    assert f"*:{port}:*:{backuprole}:{pgbackrest_password}" in passfile_entries(
        passfile, role=backuprole
    )
