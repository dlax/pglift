# SPDX-FileCopyrightText: 2021 Dalibo
#
# SPDX-License-Identifier: GPL-3.0-or-later

from __future__ import annotations

import pytest

from pglift import _install
from pglift.settings import Settings


def test_check_uninstalled(settings: Settings) -> None:
    assert not _install.check(settings)


@pytest.mark.usefixtures("installed")
def test_check_installed(settings: Settings) -> None:
    assert _install.check(settings)
