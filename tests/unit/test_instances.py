# SPDX-FileCopyrightText: 2021 Dalibo
#
# SPDX-License-Identifier: GPL-3.0-or-later

from __future__ import annotations

import logging
import pathlib
from collections.abc import Iterator
from unittest.mock import patch

import attrs
import pytest

from pglift import exceptions, instances, postgresql, ui
from pglift.models.system import Instance, PGSetting
from pglift.settings import Settings
from pglift.types import ConfigChanges


def test_system_list_no_instance(settings: Settings) -> None:
    assert list(instances.system_list(settings)) == []


def test_system_list(settings: Settings, pg_version: str, instance: Instance) -> None:
    assert list(map(str, instances.system_list(settings))) == [f"{pg_version}/test"]


def test_system_list_custom_datadir(tmp_path: pathlib.Path, settings: Settings) -> None:
    datadir = tmp_path / "{name}" / "post" / "gres" / "{version}" / "data"
    object.__setattr__(settings.postgresql, "datadir", datadir)

    i1 = pathlib.Path(str(datadir).format(name="foo", version="15"))
    i1.mkdir(parents=True)
    (i1 / "PG_VERSION").write_text("15\n")
    (i1 / "postgresql.conf").touch()
    i2 = pathlib.Path(str(datadir).format(name="bar", version="13"))
    i2.mkdir(parents=True)
    (i2 / "PG_VERSION").write_text("13\n")
    (i2 / "postgresql.conf").touch()
    assert list(map(str, instances.system_list(settings))) == ["13/bar", "15/foo"]


@pytest.fixture
def no_confirm_ui() -> Iterator[list[tuple[str, bool]]]:
    args = []

    class NoUI(ui.UserInterface):
        def confirm(self, message: str, default: bool) -> bool:
            args.append((message, default))
            return False

    token = ui.set(NoUI())
    yield args
    ui.reset(token)


def test_drop(instance: Instance, no_confirm_ui: list[tuple[str, bool]]) -> None:
    with pytest.raises(exceptions.Cancelled):
        instances.drop(instance)
    assert no_confirm_ui == [
        (f"Confirm complete deletion of instance {instance}?", True)
    ]


def test_env_for(settings: Settings, instance: Instance) -> None:
    expected_env = {
        "PGDATA": str(instance.datadir),
        "PGHOST": "/socks",
        "PGPASSFILE": str(settings.postgresql.auth.passfile),
        "PGPORT": "999",
        "PGUSER": "postgres",
        "PSQLRC": f"{instance.datadir}/.psqlrc",
        "PSQL_HISTORY": f"{instance.datadir}/.psql_history",
        "PGBACKREST_CONFIG_PATH": f"{settings.prefix}/etc/pgbackrest",
        "PGBACKREST_STANZA": "test-stanza",
    }
    assert instances.env_for(instance) == expected_env


def test_exec(settings: Settings, instance: Instance) -> None:
    with patch("os.execve", autospec=True) as patched, patch.dict(
        "os.environ", {"PGUSER": "me", "PGPASSWORD": "qwerty"}, clear=True
    ):
        instances.exec(instance, command=("psql", "--user", "test", "--dbname", "test"))
    expected_env = {
        "PGDATA": str(instance.datadir),
        "PGPASSFILE": str(settings.postgresql.auth.passfile),
        "PGPORT": "999",
        "PGUSER": "me",
        "PGHOST": "/socks",
        "PGPASSWORD": "qwerty",
        "PSQLRC": str(instance.psqlrc),
        "PSQL_HISTORY": str(instance.psql_history),
        "PGBACKREST_CONFIG_PATH": f"{settings.prefix}/etc/pgbackrest",
        "PGBACKREST_STANZA": "test-stanza",
    }

    bindir = instance.bindir
    cmd = [
        f"{bindir}/psql",
        "--user",
        "test",
        "--dbname",
        "test",
    ]
    patched.assert_called_once_with(f"{bindir}/psql", cmd, expected_env)

    with patch("os.execve", autospec=True) as patched:
        instances.exec(instance, command=("true",))
    assert patched.called

    with patch("os.execve", autospec=True) as patched, pytest.raises(
        exceptions.FileNotFoundError, match="nosuchprogram"
    ):
        instances.exec(instance, command=("nosuchprogram",))
    assert not patched.called


def test_env(settings: Settings, instance: Instance) -> None:
    bindir = instance.bindir
    with patch.dict("os.environ", {"PATH": "/pg10/bin"}):
        expected_env = [
            f"export PATH={bindir}:/pg10/bin",
            f"export PGBACKREST_CONFIG_PATH={settings.prefix}/etc/pgbackrest",
            "export PGBACKREST_STANZA=test-stanza",
            f"export PGDATA={instance.datadir}",
            "export PGHOST=/socks",
            f"export PGPASSFILE={settings.postgresql.auth.passfile}",
            "export PGPORT=999",
            "export PGUSER=postgres",
            f"export PSQLRC={instance.psqlrc}",
            f"export PSQL_HISTORY={instance.psql_history}",
        ]
        assert instances.env(instance) == "\n".join(expected_env)


def test_exists(settings: Settings, instance: Instance) -> None:
    assert instances.exists(instance.name, instance.version, settings)
    assert not instances.exists("doesnotexists", instance.version, settings)


def test_upgrade_forbid_same_instance(instance: Instance) -> None:
    with pytest.raises(
        exceptions.InvalidVersion,
        match=f"Could not upgrade {instance.version}/test using same name and same version",
    ):
        instances.upgrade(instance, version=instance.version)


def test_upgrade_target_instance_exists(instance: Instance) -> None:
    orig_instance = attrs.evolve(instance, name="old")
    with pytest.raises(exceptions.InstanceAlreadyExists):
        instances.upgrade(orig_instance, version=instance.version, name=instance.name)


def test_upgrade_confirm(
    instance: Instance,
    pg_version: str,
    no_confirm_ui: list[tuple[str, bool]],
) -> None:
    with pytest.raises(exceptions.Cancelled):
        instances.upgrade(instance, name="new", version=pg_version)
    assert no_confirm_ui == [
        (f"Confirm upgrade of instance {instance} to version {pg_version}?", True)
    ]


def test_standby_upgrade(standby_instance: Instance) -> None:
    with pytest.raises(
        exceptions.InstanceReadOnlyError,
        match=f"^{standby_instance.version}/standby is a read-only standby instance$",
    ):
        instances.upgrade(
            standby_instance, version=str(int(standby_instance.version) + 1)
        )


def test_non_standby_promote(instance: Instance) -> None:
    with pytest.raises(
        exceptions.InstanceStateError,
        match=f"^{instance.version}/test is not a standby$",
    ):
        instances.promote(instance)


def test_check_pending_actions(
    instance: Instance,
    caplog: pytest.LogCaptureFixture,
    no_confirm_ui: list[tuple[str, bool]],
) -> None:
    _settings = [
        PGSetting(
            name="needs_restart",
            context="postmaster",
            setting="somevalue",
            pending_restart=False,
        ),
        PGSetting(
            name="needs_reload",
            context="sighup",
            setting="somevalue",
            pending_restart=False,
        ),
    ]
    changes: ConfigChanges = {
        "needs_restart": ("before", "after"),
        "needs_reload": ("before", "after"),
    }

    restart_on_changes = True
    with patch.object(
        postgresql, "is_running", return_value=True, autospec=True
    ), patch("pglift.db.connect", autospec=True) as db_connect, patch.object(
        instances, "settings", return_value=_settings, autospec=True
    ) as settings, patch.object(
        instances, "reload", autospec=True
    ) as reload, caplog.at_level(
        logging.INFO
    ):
        instances.check_pending_actions(instance, changes, restart_on_changes)
    db_connect.assert_called_once_with(instance)
    assert no_confirm_ui == [
        ("PostgreSQL needs to be restarted; restart now?", restart_on_changes)
    ]
    settings.assert_called_once()
    assert (
        f"instance {instance} needs restart due to parameter changes: needs_restart"
        in caplog.messages
    )
    assert (
        f"instance {instance} needs reload due to parameter changes: needs_reload"
        in caplog.messages
    )
    reload.assert_called_once_with(instance)
