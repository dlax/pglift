# SPDX-FileCopyrightText: 2021 Dalibo
#
# SPDX-License-Identifier: GPL-3.0-or-later

import enum
import json
from datetime import date
from typing import Annotated, Any, Literal, Optional

import click
import pytest
from click.testing import CliRunner
from pydantic import AfterValidator, BaseModel, Field, SecretStr, ValidationInfo

from pglift.models import helpers
from pglift.types import AnsibleConfig, AutoStrEnum, CLIConfig

from . import click_result_traceback


class Gender(enum.Enum):
    male = "M"
    female = "F"


CountryValues = Literal["fr", "be", "gb"]


class Location(BaseModel):
    system: Literal["4326"] = Field(description="coordinates system", default="4326")
    long_: float = Field(alias="long", description="longitude")
    lat: float = Field(description="latitude")


def validate_city_and_country(
    value: CountryValues, info: ValidationInfo
) -> CountryValues:
    if value == "fr" and info.data["city"] == "bruxelles":
        raise ValueError("Bruxelles is in Belgium!")
    return value


class Address(BaseModel, extra="forbid"):
    street: list[str] = Field(description="street lines", min_length=1)
    building: Annotated[
        Optional[str], CLIConfig(hide=True), AnsibleConfig(hide=True)
    ] = Field(default=None)
    zip_code: Annotated[int, AnsibleConfig(hide=True)] = Field(
        default=0, description="ZIP code"
    )
    city: Annotated[
        str,
        CLIConfig(name="town", metavar="city"),
        AnsibleConfig(spec={"type": "str", "description": ["the city"]}),
    ] = Field(description="city")
    country: Annotated[
        CountryValues,
        AfterValidator(validate_city_and_country),
        CLIConfig(choices=["fr", "be"]),
        AnsibleConfig(choices=["fr", "gb"]),
    ] = Field()
    primary: bool = Field(
        default=False, description="is this person's primary address?"
    )
    coords: Optional[Location] = Field(default=None, description="coordinates")


class Title(AutoStrEnum):
    mr = enum.auto()
    ms = enum.auto()
    dr = enum.auto()


class PhoneNumber(BaseModel):
    label: str = Field(description="Type of phone number")
    number: str = Field(description="Number")


class BirthInformation(BaseModel):
    date_: date = Field(alias="date", description="date of birth")
    place: Optional[str] = Field(
        description="place of birth", default=None, json_schema_extra={"readOnly": True}
    )


class Person(BaseModel, extra="forbid"):
    name: str = Field(min_length=3)
    nickname: Optional[SecretStr] = None
    gender: Optional[Gender] = None
    title: list[Title] = Field(default=[])
    age: Optional[int] = Field(default=None, description="age")
    address: Optional[Address] = Field(default=None)
    birth: BirthInformation = Field(description="birth information")
    phone_numbers: list[PhoneNumber] = Field(
        default_factory=list,
        description="Phone numbers",
    )


def test_paramspec() -> None:
    foospec = helpers.ArgumentSpec(("foo",), {"type": int}, ())
    barspec = helpers.OptionSpec(("--bar",), {}, ())

    @click.command()
    @foospec.decorator
    @barspec.decorator
    def cmd(foo: int, bar: str) -> None:
        assert isinstance(foo, int)
        assert isinstance(bar, str)
        click.echo(f"foo: {foo}, bar: {bar}")

    runner = CliRunner()
    result = runner.invoke(cmd, ["1", "--bar=baz"])
    assert result.stdout == "foo: 1, bar: baz\n"


def test_parameters_from_model_typeerror() -> None:
    with pytest.raises(TypeError, match="expecting a 'person: Person' parameter"):

        @click.command("add-person")
        @helpers.parameters_from_model(Person, "create")
        @click.pass_context
        def cb1(ctx: click.core.Context, x: Person) -> None:
            pass

    with pytest.raises(TypeError, match="expecting a 'person: Person' parameter"):

        @click.command("add-person")
        @helpers.parameters_from_model(Person, "create")
        @click.pass_context
        def cb2(ctx: click.core.Context, person: str) -> None:
            pass


def test_parameters_from_model() -> None:
    @click.command("add-person")
    @click.option("--exclude-none", is_flag=True, default=False)
    @helpers.parameters_from_model(Person, "create")
    @click.option("--indent", type=int)
    @click.pass_context
    def add_person(
        ctx: click.core.Context, exclude_none: bool, person: Person, indent: int
    ) -> None:
        """Add a new person."""
        click.echo(
            person.model_dump_json(
                by_alias=True, indent=indent, exclude_none=exclude_none
            ),
            err=True,
        )

    runner = CliRunner(mix_stderr=False)
    result = runner.invoke(add_person, ["--help"])
    assert result.exit_code == 0, click_result_traceback(result)
    assert result.stdout == (
        "Usage: add-person [OPTIONS] NAME\n"
        "\n"
        "  Add a new person.\n"
        "\n"
        "Options:\n"
        "  --exclude-none\n"
        "  --nickname TEXT\n"
        "  --gender [M|F]\n"
        "  --title [mr|ms|dr]\n"
        "  --age AGE                       Age.\n"
        "  --address-street STREET         Street lines.\n"
        "  --address-zip-code ZIP_CODE     ZIP code.\n"
        "  --address-town CITY             City.\n"
        "  --address-country [fr|be]\n"
        "  --address-primary / --no-address-primary\n"
        "                                  Is this person's primary address?\n"
        "  --address-coords-long LONG      Longitude.\n"
        "  --address-coords-lat LAT        Latitude.\n"
        "  --birth-date DATE               Date of birth.  [required]\n"
        "  --birth-place PLACE             Place of birth.\n"
        "  --phone-numbers PHONE_NUMBERS   Phone numbers.\n"
        "  --indent INTEGER\n"
        "  --help                          Show this message and exit.\n"
    )

    result = runner.invoke(
        add_person,
        [
            "alice",
            "--exclude-none",
            "--age=42",
            "--gender=F",
            "--address-street=bd montparnasse",
            "--address-street=far far away",
            "--address-town=paris",
            "--address-country=fr",
            "--address-primary",
            "--address-coords-long=12.3",
            "--address-coords-lat=9.87",
            "--birth-date=1981-02-18",
            "--indent=2",
            "--nickname",
            "--title=ms",
            "--title=dr",
        ],
        input="alc\nalc\n",
    )
    assert result.exit_code == 0, click_result_traceback(result)
    assert json.loads(result.stderr) == {
        "address": {
            "city": "paris",
            "country": "fr",
            "coords": {"system": "4326", "lat": 9.87, "long": 12.3},
            "street": ["bd montparnasse", "far far away"],
            "zip_code": 0,
            "primary": True,
        },
        "age": 42,
        "birth": {"date": "1981-02-18"},
        "gender": "F",
        "name": "alice",
        "nickname": "**********",
        "title": ["ms", "dr"],
        "phone_numbers": [],
    }

    result = runner.invoke(
        add_person,
        [
            "foo",
            "--address-street=larue",
            "--address-town=laville",
            "--address-country=lepays",
        ],
    )
    assert result.exit_code == 2
    assert (
        "Error: Invalid value for '--address-country': 'lepays' is not one of 'fr', 'be'"
        in result.stderr
    )


def test_parameters_from_model_update() -> None:
    @click.command("update-person")
    @helpers.parameters_from_model(Person, "update")
    @click.pass_context
    def update_person(ctx: click.core.Context, person: Person) -> None:
        """Modify new person."""
        click.echo(person.model_dump_json(by_alias=True, exclude_unset=True), err=True)

    runner = CliRunner()
    result = runner.invoke(
        update_person,
        ["alice", "--age=5", "--birthdate=2042-02-31"],
    )
    assert result.exit_code == 2, result.output
    assert "Error: No such option: --birthdate" in result.output

    result = runner.invoke(
        update_person,
        ["alice", "--age=5", "--birth-date=1987-06-05"],
    )
    assert result.exit_code == 0, result.output
    assert json.loads(result.output) == {
        "name": "alice",
        "age": 5,
        "birth": {"date": "1987-06-05"},
    }

    result = runner.invoke(
        update_person, ["alice", "--age=abc", "--birth-date=2010-02-03"]
    )
    assert result.exit_code == 2
    assert (
        "Error: Invalid value for '--age': Input should be a valid integer"
        in result.output
    )

    result = runner.invoke(
        update_person,
        [
            "bob",
            "--birth-date=1987-06-05",
            "--address-town=laville",
            "--address-country=be",
            "--address-coords-long=123",
            "--address-coords-lat=moving",
        ],
    )
    assert result.exit_code == 2
    assert (
        "Error: Invalid value for '--address-coords-lat': Input should be a valid number"
        in result.output
    )


def test_parameters_from_model_no_parse() -> None:
    @click.command("add-person")
    @helpers.parameters_from_model(Person, "create", parse_model=False)
    @click.pass_context
    def add_person(ctx: click.core.Context, /, **values: Any) -> None:
        person = Person.model_validate(values)
        click.echo(person.model_dump_json(by_alias=True, exclude_unset=True))

    runner = CliRunner()
    result = runner.invoke(
        add_person,
        [
            "alice",
            "--age=42",
            "--gender=F",
            "--address-street=bd montparnasse",
            "--address-town=paris",
            "--address-country=fr",
            "--address-primary",
            "--birth-date=1981-02-18",
        ],
    )
    assert result.exit_code == 0, click_result_traceback(result)
    assert json.loads(result.stdout) == {
        "address": {
            "city": "paris",
            "country": "fr",
            "street": ["bd montparnasse"],
            "primary": True,
        },
        "age": 42,
        "birth": {"date": "1981-02-18"},
        "gender": "F",
        "name": "alice",
    }

    result = runner.invoke(
        add_person,
        [
            "gaston",
            "--age=123",
            "--gender=F",
            "--address-street=rue de gaston",
            "--address-town=bruxelles",
            "--address-country=fr",
            "--birth-date=1919-09-19",
        ],
    )
    assert result.exit_code == 2
    assert (
        "Error: Invalid value for '--address-country': Value error, Bruxelles is in Belgium!"
        in result.output
    )


def test_unnest() -> None:
    params = {
        "name": "alice",
        "age": 42,
        "gender": "F",
        "address_city": "paris",
        "address_country": "fr",
        "address_street": ["bd montparnasse"],
        "address_zip_code": 0,
        "address_primary": True,
        "address_coords_long": 0,
        "address_coords_lat": 1.2,
    }
    assert helpers.unnest(Person, params) == {
        "name": "alice",
        "age": 42,
        "gender": "F",
        "address": {
            "city": "paris",
            "coords": {"long": 0, "lat": 1.2},
            "country": "fr",
            "street": ["bd montparnasse"],
            "zip_code": 0,
            "primary": True,
        },
    }

    with pytest.raises(ValueError, match="invalid"):
        helpers.unnest(Person, {"age": None, "invalid": "value"})
    with pytest.raises(ValueError, match="in_va_lid"):
        helpers.unnest(Person, {"age": None, "in_va_lid": "value"})


def test_parse_params_as() -> None:
    address_params = {
        "city": "paris",
        "country": "fr",
        "street": ["bd montparnasse"],
        "zip_code": 0,
        "primary": True,
    }
    address = Address(
        street=["bd montparnasse"],
        zip_code=0,
        city="paris",
        country="fr",
        primary=True,
    )
    assert helpers.parse_params_as(Address, address_params) == address

    params = {
        "name": "alice",
        "age": 42,
        "gender": "F",
        "address": address_params,
        "birth": {"date": "1976-05-04"},
    }
    person = Person(
        name="alice",
        age=42,
        gender=Gender.female,
        address=address,
        birth=BirthInformation(date=date(1976, 5, 4)),
    )
    assert helpers.parse_params_as(Person, params) == person

    params_nested = {
        "name": "alice",
        "age": 42,
        "gender": "F",
        "birth_date": "1976-05-04",
    }
    params_nested.update({f"address_{k}": v for k, v in address_params.items()})
    assert helpers.parse_params_as(Person, params_nested) == person


def test_argspec_from_model() -> None:
    argspec = helpers.argspec_from_model(Person)
    assert argspec == {
        "name": {"required": True, "type": "str"},
        "nickname": {"no_log": True, "type": "str"},
        "title": {"default": [], "type": "list", "elements": "str"},
        "gender": {"choices": ["M", "F"]},
        "age": {"type": "int", "description": ["age"]},
        "birth": {
            "description": ["birth information"],
            "options": {
                "date": {"description": ["date of birth"], "required": True},
                "place": {"description": ["place of birth"], "type": "str"},
            },
            "required": True,
            "type": "dict",
        },
        "address": {
            "description": ["address"],
            "options": {
                "city": {
                    "description": ["the city"],
                    "required": True,
                    "type": "str",
                },
                "coords": {
                    "description": ["coordinates"],
                    "options": {
                        "lat": {
                            "description": ["latitude"],
                            "required": True,
                            "type": "float",
                        },
                        "long": {
                            "description": ["longitude"],
                            "required": True,
                            "type": "float",
                        },
                        "system": {
                            "choices": ["4326"],
                            "default": "4326",
                            "description": ["coordinates system"],
                        },
                    },
                    "type": "dict",
                },
                "country": {
                    "choices": ["fr", "gb"],
                    "required": True,
                },
                "primary": {
                    "default": False,
                    "description": ["is this person's primary address?"],
                    "type": "bool",
                },
                "street": {
                    "description": ["street lines"],
                    "elements": "str",
                    "required": True,
                    "type": "list",
                },
            },
            "type": "dict",
        },
        "phone_numbers": {
            "type": "list",
            "elements": "dict",
            "description": ["Phone numbers"],
            "options": {
                "label": {
                    "description": ["Type of phone number"],
                    "required": True,
                    "type": "str",
                },
                "number": {
                    "description": ["Number"],
                    "type": "str",
                    "required": True,
                },
            },
        },
    }


class Sub(BaseModel):
    f: int


class Nested(BaseModel):
    s: Sub


def test_argspec_from_model_nested_optional() -> None:
    """An optional nested model should propagate non-required on all nested models."""
    assert helpers.argspec_from_model(Nested) == {
        "s": {
            "description": ["s"],
            "options": {"f": {"required": True, "type": "int"}},
            "required": True,
            "type": "dict",
        }
    }

    class Model(BaseModel):
        n: Optional[Nested] = None

    assert helpers.argspec_from_model(Model) == {
        "n": {
            "description": ["n"],
            "options": {
                "s": {
                    "description": ["s"],
                    "options": {
                        "f": {
                            "required": True,
                            "type": "int",
                        }
                    },
                    "required": True,
                    "type": "dict",
                }
            },
            "type": "dict",
        }
    }


class Nested1(BaseModel):
    r: int = Field(description="Look, a.word.with.dots. And a second sentence.")
    d: int = 42


class Model(BaseModel):
    n: Optional[Nested1] = None


def test_argspec_from_model_nested_default() -> None:
    """A default value on a optional nested model should not be set as "default" in ansible"""
    assert helpers.argspec_from_model(Model) == {
        "n": {
            "description": ["n"],
            "options": {
                "d": {
                    "default": 42,
                    "type": "int",
                },
                "r": {
                    "description": ["Look, a.word.with.dots", "And a second sentence"],
                    "required": True,
                    "type": "int",
                },
            },
            "type": "dict",
        }
    }


class Nested2(BaseModel):
    f: int = 42


class Model2(BaseModel):
    n: Nested2 = Nested2()


def test_argspec_from_model_keep_default() -> None:
    """A non-required field with a default value should keep the "default" in ansible"""
    assert helpers.argspec_from_model(Model2) == {
        "n": {
            "default": {"f": 42},
            "description": ["n"],
            "options": {"f": {"default": 42, "type": "int"}},
            "type": "dict",
        }
    }
