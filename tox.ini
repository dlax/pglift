# SPDX-FileCopyrightText: 2021 Dalibo
#
# SPDX-License-Identifier: GPL-3.0-or-later

[tox]
minversion = 3.24.5
envlist = lint,typing,tests-{doctest,unit,func,expect,ansible},bandit,docs
isolated_build = true

[testenv:lint]
commands =
  black --check .
  flake8 .
  isort --check --diff .
  codespell
  reuse lint
  towncrier build --version=Unreleased --draft
  pre-commit run --all-files --show-diff-on-failure pyupgrade
deps =
  black
  codespell
  flake8
  isort
  pre-commit
  pyupgrade
  reuse
  towncrier
skip_install = true

[testenv:typing]
commands =
  mypy
extras =
  test
  typing

[testenv:tests-{doctest,unit,func}]
commands =
  pytest {posargs:--showlocals}
extras = test
changedir =
  doctest: src
  unit: tests/unit
  func: tests/func
setenv =
  func: PYTHONASYNCIODEBUG=1
passenv =
  func: DBUS_SESSION_BUS_ADDRESS
  func: XDG_RUNTIME_DIR
usedevelop = true

[testenv:tests-expect]
commands =
  prysk t
extras = test
usedevelop = true

[testenv:tests-ansible]
changedir =
  tests/ansible
commands =
  ansible-galaxy collection install git+https://gitlab.com/dalibo/pglift-ansible.git
  pytest {posargs}
deps =
  ansible
  distlib
  patroni[etcd]>=2.1.5
  port-for
  psycopg
  psycopg2-binary
  PyYAML
  pytest
  temboard-agent>=8.1.0
  trustme
setenv =
  ANSIBLE_COLLECTIONS_PATH={toxworkdir}
usedevelop = true

[testenv:tests-binary]
changedir =
  tests/ansible
allowlist_externals =
  /usr/bin/ln
commands =
  ln -rs {toxinidir}/pyoxidizer/build/x86_64-unknown-linux-gnu/release/install/pglift {envbindir}
  pglift --version
  ansible-galaxy collection install git+https://gitlab.com/dalibo/pglift-ansible.git
  pytest {posargs}
deps =
  ansible
  distlib
  patroni[etcd]>=2.1.5
  port-for
  psycopg
  psycopg2-binary
  PyYAML
  pytest
  tenacity
  temboard-agent>=8.1.0
  trustme
setenv =
  ANSIBLE_COLLECTIONS_PATH={toxworkdir}
skip_install = true

[testenv:bandit]
commands =
  bandit -c {toxinidir}/.bandit -r {toxinidir}/src
deps =
  bandit

[testenv:docs]
commands =
  sphinx-build -b html -W -T docs docs/_build
extras = docs

[testenv:pin]
commands =
  pip-compile \
    --upgrade \
    --output-file pyoxidizer/requirements.txt \
    pyproject.toml
deps =
  pip-tools >= 7.0.0
skip_install = true

[testenv:buildbin]
allowlist_externals =
  /usr/bin/ln
commands =
  pyoxidizer build --release --path pyoxidizer --system-rust
  ln -rs {toxinidir}/pyoxidizer/build/x86_64-unknown-linux-gnu/release/install/pglift {envbindir}
  pglift --version
deps =
  pyoxidizer
passenv =
  PYOXIDIZER_CACHE_DIR
skip_install = true

[testenv:release]
commands =
  git describe --exact-match
  rm -rf {toxinidir}/dist
  {envpython} -m build
  {envpython} -m twine check {toxinidir}/dist/*
  {envpython} -m twine upload --verbose {toxinidir}/dist/*
allowlist_externals =
  git
  rm
deps =
  build
  twine
skip_install = true
setenv =
  TWINE_USERNAME=__token__
  TWINE_PASSWORD={env:PYPI_TOKEN}
